using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputsManager : MonoBehaviour {

    public static PlayerInputsManager Singleton;

    private PlayerControls playerControls;
    [SerializeField] private Vector2 movementInput;
    
    public Vector2 MovementInput { get {return movementInput;} }

    void Awake() {
        
        if(Singleton == null) {

            Singleton = this;

        }
        else {

            Destroy(gameObject);

        }

    }

    void OnEnable() {

        if(playerControls == null) {

            playerControls = new PlayerControls();

            playerControls.PlayerMovement.Movement.performed += i => movementInput = i.ReadValue<Vector2>();

        }

        playerControls.Enable();
    
    } 

}
