using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocomotion : MonoBehaviour
{

    private Rigidbody2D rb;

    private void Start() {
        
        rb = GetComponent<Rigidbody2D>();

    }

    public void HandleMovement(float _deltaTime) {
        
        Move(_deltaTime);

    }

    private void Move(float _deltaTime) {
        
        rb.velocity = Vector2.Lerp(rb.velocity, PlayerInputsManager.Singleton.MovementInput * PlayerManager.Singleton.MovementSpeed, _deltaTime * PlayerManager.Singleton.AccelerationSpeed);

        if(Mathf.Abs(rb.velocity.x) < PlayerManager.Singleton.MovementThreshold) {

            rb.velocity = new Vector2(0, rb.velocity.y);

        }

        if(Mathf.Abs(rb.velocity.y) < PlayerManager.Singleton.MovementThreshold) {

            rb.velocity = new Vector2(rb.velocity.x, 0);

        }

    }

}

