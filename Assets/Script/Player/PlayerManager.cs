using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Singleton;

    private PlayerLocomotion playerMovement;
    [SerializeField] private float deltaTime; 
    [Header("Movement Settings")]
    [SerializeField] private float movementSpeed;
    [SerializeField] private float accelerationSpeed;
    [SerializeField] private float movementThreshold;

    public float MovementSpeed { get {return movementSpeed;} }
    public float AccelerationSpeed { get {return accelerationSpeed;} }
    public float MovementThreshold { get {return movementThreshold;} }

    private void Awake() {
        
        if(Singleton == null) {

            Singleton = this;

        }
        else {

            Destroy(gameObject);

        }

    }

    private void Start() {
        
        playerMovement = GetComponent<PlayerLocomotion>();

    }

    void Update() {

        deltaTime = Time.deltaTime;

        playerMovement.HandleMovement(deltaTime);

    }

}

